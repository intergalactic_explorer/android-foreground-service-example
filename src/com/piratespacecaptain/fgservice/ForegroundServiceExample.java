package com.piratespacecaptain.fgservice;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class ForegroundServiceExample extends Activity
{
  /////////////////////////////////////////////////////////////////////////////////////
  /////////////////////////////////////////////////////////////////////////////////////
  @Override
  public void onCreate(Bundle savedInstanceState)
  {
    log("onCreate()");

    super.onCreate(savedInstanceState);
    setContentView(R.layout.main);
  
    m_start_button    = (Button)   findViewById(R.id.start);
    m_stop_button     = (Button)   findViewById(R.id.stop);

    m_context = (Context) this;

    m_start_button.setOnClickListener(new View.OnClickListener() {
      public void onClick(View v) {
        log("Start!");
        
        send_service_command(ForegroundService.CMD_START);
      }
    });
    
    m_stop_button.setOnClickListener(new View.OnClickListener() {
      public void onClick(View v) {
        log("Stop!");

        send_service_command(ForegroundService.CMD_STOP);
      }
    });
  }

  /////////////////////////////////////////////////////////////////////////////////////
  /////////////////////////////////////////////////////////////////////////////////////
  @Override
  protected void onDestroy()
  {
    log("onDestroy()");
    super.onDestroy();
  }
  
 

  /////////////////////////////////////////////////////////////////////////////////////
  private Button m_start_button;
  /////////////////////////////////////////////////////////////////////////////////////
  private Button m_stop_button;

  /////////////////////////////////////////////////////////////////////////////////////
  private Context m_context;

  /////////////////////////////////////////////////////////////////////////////////////
  private static final String m_TAG = "ForegroundServiceExample";


  /////////////////////////////////////////////////////////////////////////////////////
  /////////////////////////////////////////////////////////////////////////////////////
  private void send_service_command(int command) {
    Intent command_intent = new Intent(m_context, ForegroundService.class);
    command_intent.putExtra("COMMAND", command);
    startService(command_intent);
  }
  
  /////////////////////////////////////////////////////////////////////////////////////
  /////////////////////////////////////////////////////////////////////////////////////
  private void log(String output) {
    Log.d(m_TAG, output);
  }
}
