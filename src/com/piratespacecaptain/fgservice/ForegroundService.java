package com.piratespacecaptain.fgservice;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.util.Log;
import java.lang.String;


public class ForegroundService extends Service implements Handler.Callback
{
  /////////////////////////////////////////////////////////////////////////////////////
  public static final int CMD_START = 1;
  public static final int CMD_STOP  = 2;

  
  /////////////////////////////////////////////////////////////////////////////////////
  /////////////////////////////////////////////////////////////////////////////////////
  @Override
  public void onCreate() {
    log("onCreate()");
  
    m_notification_manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE); 

    m_state = STATE_STOPPED;
  }
  
  /////////////////////////////////////////////////////////////////////////////////////
  /////////////////////////////////////////////////////////////////////////////////////
  @Override
  public void onDestroy() {
    log("onDestroy()");
  }

  /////////////////////////////////////////////////////////////////////////////////////
  /////////////////////////////////////////////////////////////////////////////////////
  @Override
  public boolean onUnbind(Intent intent) {
    log("onUnbind()");
    // Not used //
    return false;
  }

  /////////////////////////////////////////////////////////////////////////////////////
  /////////////////////////////////////////////////////////////////////////////////////
  @Override
  public IBinder onBind(Intent intent) {
    log("onBind()");
    // Not used //
    return m_binder;
  }

  /////////////////////////////////////////////////////////////////////////////////////
  /////////////////////////////////////////////////////////////////////////////////////
  @Override
  public int onStartCommand(Intent intent, int flags, int startId) {
    log("onStartCommand()");

    // Process Command //
    switch(intent.getIntExtra("COMMAND", 0)) {
      
      case CMD_START:
        log("CMD_START");

        switch(m_state) {
          case STATE_STARTED:
            log("Service already started.");
            break;

          case STATE_STOPPED:
            log("Starting service...");
            start();
            break;
        }
        break;

      case CMD_STOP:
        log("CMD_STOP");
        
        switch(m_state) {
          case STATE_STARTED:
            log("Stopping service...");
            stop(); 
            break;

          case STATE_STOPPED:
            log("Service already stopped.");
            break;
        }
        break;

      case 0:
        log("Error: Command value not set.");
        break;
    }

    return START_NOT_STICKY;
  }

  /////////////////////////////////////////////////////////////////////////////////////
  /////////////////////////////////////////////////////////////////////////////////////
  public boolean handleMessage(Message message) {
    return true;
  }

 

  /////////////////////////////////////////////////////////////////////////////////////
  private int              m_state;
  private static final int STATE_STOPPED = 0;
  private static final int STATE_STARTED = 1;

  /////////////////////////////////////////////////////////////////////////////////////
  private WorkerThread m_worker_thread;

  /////////////////////////////////////////////////////////////////////////////////////
  private NotificationManager m_notification_manager;
  
  /////////////////////////////////////////////////////////////////////////////////////
  private final IBinder m_binder = new Messenger(new Handler(this)).getBinder();

  /////////////////////////////////////////////////////////////////////////////////////
  private static final String m_TAG = "ForegroundService";



  /////////////////////////////////////////////////////////////////////////////////////
  /////////////////////////////////////////////////////////////////////////////////////
  private void stop() {
    log("stop()");
    m_worker_thread.kill();
    m_notification_manager.cancelAll();
    
    stopForeground(true);
    m_state = STATE_STOPPED;
  }

  /////////////////////////////////////////////////////////////////////////////////////
  /////////////////////////////////////////////////////////////////////////////////////
  private void start() {
    log("start()");

    // Start service as foreground service //
    Notification notification   = new Notification(R.drawable.gear, "Start!", System.currentTimeMillis());
    Intent notificationIntent   = new Intent(this, ForegroundServiceExample.class);
    PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent,0);
    notification.setLatestEventInfo(this, "Foreground Service Example", "", pendingIntent);
    startForeground(1, notification);
    
    // Start worker thread //
    m_worker_thread = new WorkerThread();
    m_worker_thread.start();
    
    m_state = STATE_STARTED;
  }

  
  /////////////////////////////////////////////////////////////////////////////////////
  /////////////////////////////////////////////////////////////////////////////////////
  private void log(String output) {
    Log.d(m_TAG, output);
  }
}
